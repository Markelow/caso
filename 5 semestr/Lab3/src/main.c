#include <reg51.h>
#include <math.h>

/*Float point*/
/*
void main() 
{
	double x, y;
	while (1)
	{
		for (x = -1.0; x <= 1.0; x+=0.01)
		{
			P2 = 0;
			y = x + x * x * x / 6 + 3 * x * x * x * x * x / 40 + 15 * x * x * x * x * x * x * x / 336;
			P2 = 1;
			P3 = y*77 + 100;
		}
	}
}
*/
/*Decimal*/
/*
void main() 
{
	unsigned int x, y, a, m = 100;
	while (1)
	{
		for (x = 0; x <= m; x++)
		{
			P2 = 0;
			a = x * x / m;	
			y = x * (m + a *(m / 6 + a * (m * 3 / 40 + a * 15 / 336) / m) / m) / m;
			P2 = 1;
			P3 = y;
		}
	}
}
*/
/*Binary*/
/*
void main() 
{
	unsigned int x, y, a, m = 256;
	while (1)
	{
		for (x = 0; x <= m; x++)
		{
			P2 = 0;
			a = (x * x) >> 8;	
			y = a * (m * 0x03 / 0x28 + a * 0x0F / 0x150) >> 8;
			y = a *(m / 0x06 + y) >> 8;
			y = (x * (m + y)) >> 8;
			P2 = 1;
			P3 = y;
		}
	}
}
*/
/*Exclude division*/
void main() 
{
	unsigned int x, y, a, m = 256;
	while (1)
	{
		for (x = 0; x <= m; x++)
		{
			P2 = 0;
			a = (x * x) >> 8;	
			y = a * ((m * 0x03 * 0x06) >> 8 + (a * 0x0F) >> 8) >> 8; 	// 0x28 ~ 0xFF / 0x06; 0x150 ~ 0xFF / 0x01;
			y = a *((m * 0x2B) >> 8 + y) >> 8; 												// 0x06 ~ 0xFF / 0x2B
			y = (x * (m + y)) >> 8;
			P2 = 1;
			P3 = y;
		}
	}
}