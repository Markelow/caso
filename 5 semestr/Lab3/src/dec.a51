;Prelude and explanations:
;max a = 100*100/100 = 100 => only 1 byte is used.
;max a * 15 / 336 = 100 * 15 / 336 = 4 => only 1 byte is used.
;constants in this function:
;m*3/40 = 100 * 3 / 40 = 7 = 0x07;
;m/6 = 100 / 6 = 16 = 0x10
EXTRN CODE (?C?UIDIV)

MOV R1, #0x00	;Start X

main_loop:

MOV A, R1		;x^2 = R6.R7
MOV B, R1
LCALL do_math
MOV R2, A		;SAVE 'a'

MOV B, #0x0F 		;(m * 3 / 40 + a * 15 / 336)
MUL AB
MOV R6, B
MOV R7, A
MOV R4, #0x01
MOV R5, #0x50
LCALL ?C?UIDIV
MOV A, R7
ADD A, #0x07

MOV B, R2		;a * (...) / m
LCALL do_math

ADD A, #0x10	;a * (m / 6 + ...) / m
MOV B, R2
LCALL do_math

ADD A, R2		;x * (m + ...) / m
MOV B, R1
LCALL do_math

MOV P3, R7		;output

INC R1
MOV A, #0x65
CLR C
SUBB A, R1
JNZ main_loop
JMP fin

do_math: 			;doing A * B / m
	MUL AB
	MOV R6, B		;Variable 'a' assigned to Register 'R6/R7'
	MOV R7, A
	MOV R4, #0x00	;Variable 'b' assigned to Register 'R4/R5'
	MOV R5, #0x64
	LCALL ?C?UIDIV	;Variable 'result' assigned to Register 'R6/R7'
	MOV A, R7
	RET
	
fin:
END

