dseg at 0x08
	S:DS 4
cseg at 0
	MOV R0, P1
	MOV A, R0
	SWAP A
	ANL A, #0x0F
	MOV B, #0x0A
	MUL AB
	MOV R1, A
	MOV A, R0
	ANL A, #0x0F
	ADD A, R1
	;Div of an elder part
	MOV B, #0x64
	DIV AB
	MOV R0, A
	MOV A, B
	;Div of a smaller part
	MOV S, #0x00
	MOV S+0x01, A
	MOV R1, #0x64
	MOV R2, #0x09
	loop:
	MOV a, S+0x01
	CLR c
	SUBB a, R1
	JNC nextbit
	MOV a, S+0x01
	RL a;
	MOV S+0x01, a
	MOV a, S
	CLR c
	RLC a
	MOV S, a
	CLR a
	ADDC a, S+0x01
	MOV S+0x01, a
	JMP cont
	nextbit:
	RL a
	MOV S+0x01,a
	MOV a, S
	CLR c
	RLC a
	INC a
	MOV S, a 
	CLR a
	ADDC a, S+0x01
	MOV S+0x01, a
	cont:
	DJNZ R2, loop
	MOV R1, S;
	;R0.R1 is a value fixed-pointed number.
	;Mult on 10.
	MOV A, R0
	MOV B, #0x0A
	MUL AB
	MOV R7, A
	MOV A, R1
	MOV B, #0x0A
	MUL AB
	MOV R6, A
	MOV A, B
	ADD A, R7
	MOV R7, A
	;R7.R6 = R0.R1 * 10
	ANL A, #0x0f
	MOV R7, A
	MOV A, R6
	MOV B, #0x0A
	MUL AB
	MOV A, B
	SWAP A
	ORL A, R7
	SWAP A
	MOV P2, A
END