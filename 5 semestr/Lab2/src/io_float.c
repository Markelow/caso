#include <reg51.h>
void main()
{
	unsigned int B;
	unsigned int y;
	B = ((P1 & 0xf0) << 4) * 10 + ((P1 & 0x0f) << 8);
	B = B / 100; 
	//In variable B we have a value of fixed-pointed number.
	y = B * 10;
	P2 = ((y & 0xf00) >> 4);  
	P2 |= (((y & 0xff) * 10) & 0xf00) >> 8; 
}