dseg at 0x08
	S:DS 4
cseg at 0
	MOV R0, P1
	LCALL f2d
	MOV R2, A
	MOV R0, P0
	LCALL f2d
	MOV B, #0x64
	MUL AB
	CLR C
	ADD A, R2
	MOV R0, A
	MOV A, B
	ADDC A, #0x00
	MOV R1, A
	;R1.R0 is a decimal value.
	MOV S, R0
	MOV S+0x01, R1
	MOV R3, #0x64
	MOV R4, #0x09
	loop:
	MOV a, S+0x01
	CLR c
	SUBB a, R3
	JNC nextbit
	MOV a, S+0x01
	RL a;
	MOV S+0x01, a
	MOV a, S
	CLR c
	RLC a
	MOV S, a
	CLR a
	ADDC a, S+0x01
	MOV S+0x01, a
	JMP cont
	nextbit:
	RL a
	MOV S+0x01,a
	MOV a, S
	CLR c
	RLC a
	INC a
	MOV S, a 
	CLR a
	ADDC a, S+0x01
	MOV S+0x01, a
	cont:
	DJNZ R4, loop
	MOV R5, S;
	MOV a, S+0x01
	RR a
	MOV R6, a
	;R5 is a result of division abd R6 is a rest
	MOV A, R5
	LCALL fd2
	MOV P2, A
	MOV A, R6
	LCALL fd2
	MOV P3, A
	JMP fin
	f2d:
	MOV A, R0
	SWAP A
	ANL A, #0x0F
	MOV B, #0x0A
	MUL AB
	MOV R1, A
	MOV A, R0
	ANL A, #0x0F
	ADD A, R1
	RET
	fd2:
	MOV B, #0x0A
	DIV AB
	ANL A, #0x0F
	SWAP A
	ORL A, B
	RET
fin:
END