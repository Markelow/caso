ad1 equ r2
ad2 equ r3
sum equ r4
carry equ r5
resultPosit equ r6	
	
Dseg at 0x10
	i : ds 1
	j : ds 1
	nt : ds 1

Xseg at 0
	result : ds 7	
	
Cseg at 0
	jmp start
	
	first  : db "256.54",0
	second : db "75.56",0
		
		
start:
	mov nt, 0
	;get first length
	mov dpl, #first
	lcall strLen 
	mov i,r0
	
	;get second length
	mov dpl, #second
	lcall strLen 
	mov j,r0
	mov ad2,#0x0
	mov carry, #0x0
	mov resultPosit, #0x6
	mainLoop:
		mov a, i
		mov dptr, #first
		movc a, @a + dptr
		mov r1,a
		cjne a, #0x2E, numb ; if ',' != first[i]
		jmp point
		numb:
			; a = (i >= 0) ? first[i]  - '0' : 0;
			mov a, i
			lcall getNum
			mov a,r1
			mov ad1,a
			;b = (j >= 0) ? second[j] - '0' : 0;
			mov a, j
			mov dptr, #second
			movc a, @a + dptr
			mov r1,a
			mov a,j
			lcall getNum
			mov a,r1
			mov ad2,a
			;sum = carry + a + b;
			add a,ad1
			add a,carry
			;if (sum > 9)
			mov sum,a
			subb a,#0xA
			jc clear_carry
				mov sum, a
				mov carry, #0x01
				jmp save_result
			clear_carry:
				mov carry,#0x00
			;result[resultPosition] = sum + '0';
			save_result:
				mov a, #0x30
				add a,sum
				mov sum,a
				mov a, #result
				add a,resultPosit
				mov r0,a
				mov a, sum
				movx @r0, a
				jmp step
		;result[resultPosition] = '.';
		point:
			mov a, #result
			add a,resultPosit
			mov r0,a
			mov a, #0x2E
			movx @r0, a
		step:
		;decriments
		dec i
		dec j
		dec resultPosit
		;while (0 <= resultPosition);
		CJNE resultPosit,#0x00,mainLoop
stop:
	jmp stop

strLen:
	mov r0, #0xFF
loop:
	inc r0
	mov a, r0
	movc a, @a + dptr ; in accum i value
	;inc dptr
	jnz loop
	dec r0
ret

;(i >= 0) ? first[i]  - '0' : 0;
getNum:
	rlc a
	jc zero1
	mov a,r1
	subb a, #0x30
	mov r1,a
	jmp exit
	zero1:
		mov r1,#0x0
	exit:
ret

end
	