#include <reg51.h>
#include <string.h>

char code first[] = "256.54";
char code second[] = "75.56";

char xdata result[7];

main()
{
	char i, j, a, b, sum, resultPosition, carry;
	i = strlen(first);
	j = strlen(second);
	i--;
	j--;
	carry = 0;
	resultPosition = 6;
	do
	{
		if ('.' != first[i])
		{
			a = (i >= 0) ? first[i]  - '0' : 0;
			b = (j >= 0) ? second[j] - '0' : 0;
			sum = carry + a + b;
			if (sum > 9)
			{
				sum -=10;
				carry = 1;
			}
			else
			{
				carry = 0;
			}
			result[resultPosition] = sum + '0';
		}
		else
		{
			result[resultPosition] = '.';
		}
		i--;
		j--;
		resultPosition--;
	}
	while (0 <= resultPosition);
	while (1);
}