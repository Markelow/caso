#include <reg51.h>
void main(void)
{
	unsigned long S;
	unsigned int A;
	char i;
	S = P0;
	S += P1 << 8;
	A = P2 << 8;
	for (i = 0; i < 9; i++)
	{
		S = (S >= A) ? ((S - A) << 1) + 1 : S << 1;
	}
	P0 = S;
	P1 = S >> 9;
}