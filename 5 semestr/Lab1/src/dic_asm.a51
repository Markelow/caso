dseg at 0x08
	S:DS 4
cseg at 0
	;get data
	MOV S, P0
	MOV S+0x01, P1
	MOV R1, P2
	MOV R2, #0x09
	loop:
		;test substraction 
		CLR C
		MOV a, S
		SUBB a,R1
		MOV a, S+0x01
		;carry from high
		SUBB a, #0x0
			
		JNC sub_shift_inc
		;simple shift
		CLR c
		MOV a, S+0x01
		RLC	a
		MOV S+0x01,a
		CLR c
		MOV a, S
		RLC a
		MOV S, a
		CLR a
		ADDC a, S+0x01
		MOV S+0x01, a
		JMP cont
	sub_shift_inc: 
			;S-A
			CLR C
			MOV a, S
			SUBB a,R1
			MOV S, a
			MOV a, S+0x01
			SUBB a, #0x0
			MOV a, S+0x01
			;(S-A)<<1
			CLR c
			MOV a, S+0x01
			RLC	a
			MOV S+0x01,a
			CLR c
			MOV a, S
			RLC a
			;inc 
			INC a
			MOV S, a
			CLR a
			ADDC a, S+0x01
			MOV S+0x01, a			
		cont:
			DJNZ R2, loop
			MOV P0, S;
			MOV a, S+0x01
			RR a
			MOV P1, a
END