dseg at 0x08
	S:DS 4
cseg at 0
start:
	MOV S, P0
	MOV R0, P1
	;constant
	MOV R2, #0x01
	MOV R1, #0x08
	MOV R4, #0x80
	circle:
		MOV a, S
		;MOV R3, a
		;litle bit to acc
		ANL a, R2
		;if s & 1 == 1
		CJNE a, #0x01, shift
		
		MOV a, R0
		;
		ADD a, S+0x01
		MOV S+0x01,a
		CLR a
		;magic addc
		ADDC a, S+0x02
		MOV S+0x02, a
		shift:
			;zero byte
			MOV a, S
			RR a
			ANL a, #0x7F
			MOV S, a
			;first byte
			MOV a, S+0x01
			CLR c
			RRC a
			MOV S+0x01, a
			JNC next1 
			MOV a, S
			ORL a, R4
			MOV S, a
		next1:
			MOV a, S+0x02
			CLR c
			RRC a
			ANL a, #0x7F
			MOV S+0x02, a
			JNC next2
			MOV a, S+0x01
			ORL a, R4
			MOV S+0x01, a
	next2:
		DJNZ R1, circle
		MOV P3, S+0x01
		MOV P2, S
jmp start
END