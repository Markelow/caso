#include <reg51.h> 
typedef unsigned char uchar;
typedef unsigned int uint;
//Functions that can work with 1 byte BCD numbers.
//Test 0xC973 (number in 16CC is number in 10CC.
//0xC973(BCD) = 12973(10) = 0x32AD
uchar fd2(uchar x)
{
	return (x >> 4) * 10 + (x & 0x0f); 
}

uchar f2d(uchar x)
{
	return  ((x / 10) << 4) | (x % 10); 
}
//Working with 2 bytes BCD numbers.
main()
{ 
	uint N;
	N = fd2(P0) * 100 + fd2(P1);
	//In variable N we have a decimal number. 
	P2 = f2d(N / 100); 
	P3 = f2d(N % 100);
	}