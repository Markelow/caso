#include <reg51.h>

void main(void)
{
	long S;
	unsigned int A;
	char i;
	S = P0;
	A = P1 << 8;
	for (i = 0; i < 8; i++)
	{
		S = (S & 1)? (S + A) >> 1 : S >> 1;
	}
	P2 = S >> 8;
	P3 = S;
} 