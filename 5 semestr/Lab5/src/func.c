#include <reg51.h>

// z = (x1y1 v !x2y2) (!x2 v y1)

char bdata byte;

sbit y1 = byte^0;
sbit y2 = byte^1;
sbit x1 = byte^2;
sbit x2 = byte^3;

sbit zLow = P2^7;
sbit zHigh = P3^7;

void main()
{
	//8 low bits
	for (byte=0, P2 = 0; byte < 8; byte++)
	{
		P2 >>=1;
		zLow =(x1 & y1 | !x2&y2) & (!x2 | y1);
	}
	//8 high bits
	for (P3 = 0; byte < 16; byte++)
	{
		P3 >>=1;
		zHigh =(x1 & y1 | !x2&y2) & (!x2 | y1);
	}
	while(1);
}