iseg at 30h
Stack: ds 10
cseg at 0
jmp start

cseg at 0Bh
jmp ex00

cseg at 40h
start: 
	mov SP, #Stack-1
	mov TMOD, #22	
	setb ET0
	setb TR0
	setb ET1
	setb TR1
	setb EA
	mov TH0,#255
	mov TL0,#255
	loop:
		mov P1, TL1
		mov P2, TH1
		jmp loop

ex00:
	push ACC
	push PSW
	mov R1, TH1
	mov R0, TL1
	mov TL1, #0
	mov TH1, #0
	pop PSW
	pop ACC
	reti

end