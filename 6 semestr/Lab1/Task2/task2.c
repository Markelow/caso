#include <reg51.h>

unsigned char mult;
unsigned int time;

//Positive length
/*intt0() interrupt 1
{ 
}

main()
{ 
TMOD=9; //16-bit mode for TIMER0 
ET0=1; 
TR0=1; 
EA=1;
while(1) {
if (P1 == TL0)
{
time = TH0 * 256 + TL0;
TL0 = 0;
TH0 = 0;
}
P1=TL0;
P2=TH0;

}
}*/

//Period lenght
intt0() interrupt 1
{
	time = TH1 * 256 + TL1;
	TL1=0;
	TH1=0;
}

main()
{ 
	TH0=255;
	TL0=255;
	TMOD=22; //16-bit mode for TIMER0 
	ET0=1;
	ET1=1; 
	TR0=1;
	TR1=1; 
	EA=1;
	while(1) 
	{
		P1=TL1;
		P2=TH1;
	}
}