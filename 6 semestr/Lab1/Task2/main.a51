iseg at 30h
Stack: ds 10
cseg at 0
jmp start

cseg at 0Bh
jmp ex00

cseg at 40h
start: 
	mov SP, #Stack-1
	mov TMOD, #9h	
	setb ET0
	setb TR0
	setb EA
	loop:
		mov a, TL0
		cjne a, P1,next
		mov R2, TH0
		mov R1, TL0
		mov TL0, #0
		mov TH0, #0
		next:
			mov P1, TL0
			mov P2, TH0
		jmp loop

ex00:
	push ACC
	push PSW
	pop PSW
	pop ACC
	reti

end