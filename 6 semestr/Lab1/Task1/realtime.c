#include <reg51.h>

unsigned    char sec,min;   
unsigned   int count=0;

//First method
/*intt0() interrupt 1
{ 
	//1 second = 12MHz / 12 pulses = 12.000.000 / 12 = 1.000.000 cycles
	//Counter is 16 bit => 65535 is MAX value
	//We need to count++ every 50.000 cycle 20 times to measure 1 second
	//65535 - 60* 256 - 175 = 50000
	TH0 = 60;
	TL0 = 175;     
  count++;
	if (count == 20) {
		sec++;
		count=0;
	} 
	if (sec == 60) {
		min++; 
		sec=0;
	} 
}

main()
{ 
	TMOD=1;	//16-bit mode for TIMER0     
  ET0=1;      
  TR0=1;      
  EA=1;       
  while(1) {
		P1=sec;
		P2=min; 
	}
}*/

//Second method
/*intt0() interrupt 1
{ 
	//1 second = 12MHz / 12 pulses = 12.000.000 / 12 = 1.000.000 cycles
	//Counter is 8 bit => 255 is MAX value
	//If we will count from 6 to 255 we need to count++ 1.000.000 / 250 = 4000 times;	
  count++;
	if (count == 4000) {
		sec++;
		count=0;
	} 
	if (sec == 60) {
		min++; 
		sec=0;
	} 
}

main()
{ 
	TMOD=2; //8-bit auto-reload mode for TIMER0
  ET0=1;      
  TR0=1;      
  EA=1;
	TH0=6; //Reload value for TL0	
  while(1) {
		P1=sec;
		P2=min; 
	}
}*/

//Third method
intt0() interrupt 1
{ 
	//If we will count from 0 to 255 we need to count++ 1.000.000 / 256 = 3906 times;	
  count++;
	if (count == 3906) {
		sec++;
		count=0;
	} 
	if (sec == 60) {
		min++; 
		sec=0;
	} 
}

main()
{ 
	TMOD=3; //8-bit split mode for TIMER0
  ET0=1;      
  TR0=1;      
  EA=1;
	while(1) {
		P1=sec;
		P2=min; 
	}
}