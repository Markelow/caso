iseg at 30h
Stack: ds 10
cseg at 0
jmp start

cseg at 0Bh
jmp ex00

cseg at 40h
start: 
mov P1, #0h		//seconds
mov P2, #0h		//minutes
mov R1, #0h		//counter
mov SP, #Stack-1
mov TMOD, #1h	//16-bit TIMER0 mode
setb ET0
setb TR0
setb EA
mov TH0, #3Ch
mov TL0, #0AFh
loop:
jmp loop

ex00:
push ACC
push PSW
mov TH0, #3Ch
mov TL0, #0AFh
mov a, R1
inc a
mov R1, a
cjne a, #14h, continue		//if != 20
mov R1, #0h
mov a, P1
inc a
mov P1, a
cjne a, #3Ch, continue		//if != 60
mov P1, #0h
mov a, P2
inc a
mov P2, a
continue:
pop PSW
pop ACC
reti

end