iseg at 30h
Stack: ds 10
cseg at 0
jmp start

cseg at 0Bh
jmp ex00

cseg at 40h
start: 
mov P1, #0h		//seconds
mov P2, #0h		//minutes
mov R1, #0h		//counter in R1 and R2
mov R2, #0h
mov SP, #Stack-1
mov TMOD, #3h	//8-bit TIMER0 mode
setb ET0
setb TR0
setb EA
loop:
jmp loop

ex00:
push ACC
push PSW
clr C
mov a, R1
add a, #1h
mov R1, a
mov a, R2
addc a, #0h
mov R2, a
//3906 base 10 = 0F42h
cjne a, #0Fh, continue		//if Hi != 0F
mov a, R1
cjne a, #42h, continue 		//if Lo != 42
mov R1, #0h
mov R2, #0h
mov a, P1
inc a
mov P1, a
cjne a, #3Ch, continue		//if != 60
mov P1, #0h
mov a, P2
inc a
mov P2, a
continue:
pop PSW
pop ACC
reti

end