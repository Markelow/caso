#include <reg51.h>
#include <intrins.h>  

unsigned codeIndex, keyCode, digit, scale=0;   
int sign = 1;
char codes[10]; 			
float number; 		
//-801
unsigned char keyboard[] = {0xED, 0xDB, 0xE7, 0x77, 0xDE}; 
//-801.9
//unsigned char keyboard[] = {0xED, 0xDB, 0xE7, 0x77, 0xEB, 0xDD, 0xDE}; 
unsigned char keyIndex;

char scan(void)
{           
		if(codeIndex==0)    //on scanning start 
		{
			number=0;   //initialize number
			scale=0;  
		}     
		//convert keyCode to digit
    switch (keyCode)    
    {    
			case 0x77:
				digit = '1';
				break;
			case 0x7B:
				digit = '2';
				break;
			case 0x7D:
				digit = '3';
				break;
			case 0xB7:
				digit = '4';
				break;
			case 0xBB:
				digit = '5';
				break;
			case 0xBD:
				digit = '6';
				break;
			case 0xD7:
				digit = '7';
				break;
			case 0xDB:
				digit = '8';
				break;
			case 0xDD:
				digit = '9';
				break;
			case 0xDE:
				digit = 'e';
				break;
			case 0xE7:
				digit = '0';
				break;
			case 0xEB:
				digit = ',';
				break;
			case 0xED:
				digit = '-';
				break;
       default: 
				digit=0xff;
				break;
    }    
    //check digit
    if(digit == 0xff) { return digit; }
		//starting scaling
    if(digit == ',')                       
    {
				scale = 1;
        codes[codeIndex++] = digit;
        codes[codeIndex] = 0;
        return digit;
    }
		if (digit == '-')
		{
			sign *= -1;
			return digit;
		}
    if(digit == 'e')    //finish 
    { 
				if (scale) { number = number / scale;}
				number *= sign;
        codeIndex = 0;
        scale = 0;
				keyIndex = 0;
    }    
    else
    {
			  codes[codeIndex++] = digit;
        number = number*10 + (digit & 0xF);        
        if (scale) { scale *= 10; }
    } 
		return digit;    
}

void InterruptHanlder(void) interrupt 0
{
    keyCode = keyboard[keyIndex++];
    if( (keyCode & 0xF0) != 0xF0 & (keyCode & 0x0F) != 0x0F)
    {
				scan(); 		//scan digit
				IE0  = 0;
		}
}

main ()
{ 
		IT0 = 1;   // Configure interrupt 0 for falling edge on /INT0 (P3.2)
		EX0 = 1;   // Enable EX0 Interrupt
		EA = 1;    // Enable Global Interrupt Flag
		keyIndex = 0;
		//we will be together forever
    while (1)
    {
    }
}