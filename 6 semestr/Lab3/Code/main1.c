#include <reg515.h>	

int mm;
unsigned int T;
unsigned char max,min;
unsigned char is_finished;
unsigned char adc(void);

main()
{ 
	is_finished = 1;
	max=0; min=0x70;
	while(is_finished)
  { 
		if (adc()>max) max=P2; 
	  if (adc()<min) min=P2;
	} 				   
	mm=(max+min)/2;	       
  TMOD=1;
  TH0 = TL0 = 0;		
	TR0=0;
  while(adc()>=mm) ;
  while(adc()<=mm);
	TR0=1;
	while(adc()>=mm);
  TR0=0;
  T =(TH0<<8)+TL0;
	T *=2;
	while(1);
}

void handler () interrupt 0 {
	is_finished = 0;
}

unsigned char adc(void)
{
	unsigned char x;
  DAPR=0;  
  while(BSY);
  return P2 = x = ADDAT; 
}
